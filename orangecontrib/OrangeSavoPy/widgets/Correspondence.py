from PyQt4 import QtCore, QtGui
from Orange.widgets.widget import OWWidget, Input, Output
from Orange.widgets import gui

import numpy as np


class MyWidget(OWWidget):
    # Widget needs a name, or it is considered an abstract widget
    # and not shown in the menu.
    name = "Correspondence Analysis"
    icon = "icons/Correspondence.svg"
    want_main_area = False
    description = "Simple correspondence analysis of a contingency table"
    
    class Inputs:
        table = Input("Contingency table", np.array)

    want_main_area = False

    def __init__(self):
        super().__init__()
        
        self.data = None
        #self.table = None
    
        #self.label = gui.widgetLabel(self.controlArea, "The size of the table is: ??")
        label = QtGui.QLabel("Correspondence Analysis")
        self.controlArea.layout().addWidget(
            label, QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
            
    @Inputs.table
    def set_table(self, table):
        """Set the contingency table."""
        self.data = table
        if self.data is None:
            #self.label.setText("The number is: ??")
            print("nsnsnsns")
        else:
            #self.label.setText("The number is {}".format(self.number))
            print(self.data.shape)
